+++
title = "Lecture notes for 'The arithmetic of the Langlands program'"
template = "arila-template.html"
+++

I am trying to write lecture notes for the ongoing course [The arithmetic of the Langlands program][AC] by A. Caraiani.
There will be two versions of them, one **stable** release and one **nightly** release, the difference being that I'll try to update the nightly version somewhat regularly, and the stable version only when I'm satisfied with their current state. 

So expect the nightly versions to change _drastically_ and without notice, and also to contain more errors (for which I am the only one to blame).

I will also add some material here and there that I've found useful, so these notes are maybe not super representative of what happened in the actual lectures.

I apologize in advance if you got stuck on any errors or inaccuracies I introduced.

Please visit the [git-repository of these notes][gitnotes] for further information.

<center>

| Release  | current version | Link     |
|----------|-----------------|----------|
| stable   |`n/a`            |[stable]  |
| nightly  | `v0.0.3`        |[nightly] |

</center>

[AC]:       https://www.ma.imperial.ac.uk/~acaraian/alp.pdf
[nightly]:  https://aaronwild.gitlab.io/arila22/arila_Nightly.pdf
[stable]:   https://aaronwild.gitlab.io/arila22/arila.pdf
[gitnotes]: https://gitlab.com/aaronwild/arila22
