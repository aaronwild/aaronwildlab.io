+++
title     = "misc"
template  = "misc.html"
+++
If you want to meet me on campus, this is my (tentative) timetable for next semester (Winter 23/24):

<center>

 ~ | Mo                | Tue             | Wed             | Thu            | Fri             |
---| ------------------|-----------------|-----------------|----------------|-----------------|
8  |   Tut             |                 |                 |                |  FA             |
10 |                   |                 | [Analytic][2]   |                |  [Analytic][2]  |
10 |                   |                 | RepII, FA       |  Meet          |                 |
12 |                   |                 |                 |                |  RepII          |
2  |                   |                 |                 |                |                 |
4  |                   |                 |                 |  HiSeg         |                 |
6  |                   |                 |                 |                |                 |


</center>


[ybonn]: https://www.mpim-bonn.mpg.de/node/11842

[kshift]: https://redshift.mathi.uni-heidelberg.de/


[2]: https://people.mpim-bonn.mpg.de/scholze/AnalyticStacks.html

