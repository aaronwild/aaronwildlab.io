+++
title     = "contact"
template  = "contact.html"
+++
email-address: `aaron.wild[at]posteo.net`

PGP fingerprint: `CEE4 4A99 F89D 6BA6 4CEE  B0AF A5C4 3D3E EE67 700C`

PGP public key: [download](/aaron_wild.key)

matrix: `@vantablue_:matrix.org`

gitlab: [aaronwild](https://gitlab.com/aaronwild)

