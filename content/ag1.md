+++
title = "Algebraic Geometry 1 (2022/23)"
template = "misc.html"
+++
Supplementary material for the tutorial sessions:
* [Session 1][1]

[1]: https://aaronwild.gitlab.io/agi/1.pdf
