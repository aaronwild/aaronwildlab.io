+++
title = "writing"
template = "writing.html"
+++
## master thesis ##

In my master thesis, I calculate the étale cohomology (with torsion coefficients) of Fargues-Scholze's stack \\( \mathrm{Bun}_G \\) for \\( G = \mathrm{GL}_2 \\).
More precisely, I can show that the inclusion \\(\mathrm{Bun}_2^{\mathrm{ss}} \hookrightarrow \mathrm{Bun}_2 \\) of the semi-stable locus induces an isomorphism on étale cohomology. 
This calculation involves a proof that the dualizing complex \\( R(\mathrm{Bun}_2 \to \mathrm{Spd}(\overline{\mathbb{F}}_q))^{!}\Lambda\\) is given just by \\( \Lambda \\) itself.
I am currently trying to remove the errors in a previous version, and will upload a new one as soon as this is done.

## lecture notes ##

* [Condensed Mathematics][CM]\
  (Lecture notes for a lecture by A.C. Le Bras, LAGA, Summer 2022)

* [Hochschild (Co)Homology][HC]\
  (Lecture notes for a lecture by P. Belmans, Bonn, Summer 2021)

## seminar talks ## 

* [Waldhausen \\(S\\)-construction][waldhausen]\
  (Graduate Seminar on Higher Segal spaces, October 2023)

* [Cohomology of Galois Gerbes][galgerb]\
  (Graduate Seminar on the local Langlands conjecture for non quasi-split groups, July 2023)

* [Motivic aspects of mixed Hodge theory][motsem]\
  ([Graduate Seminar][huysem] on Motivic aspects of Hodge theory, June 2023)

* [Tannakian Reconstruction and semi-simplicity of the Satake category][sat]\
  ([Kleine AG][klag] on Satake equivalence, March 2023)

[CM]:     https://pankratius.gitlab.io/condensed22/condensed.pdf
[HC]:     https://pankratius.gitlab.io/hochschild/hochschild.pdf
[hcnew]:     https://aaronwild.gitlab.io/hschild-21/hochschild.pdf
[AG1]:    /ag1
[arila]:  /notes-arila
[sat]:    https://aaronwild.gitlab.io/kleineag23/tannakian-satake.pdf
[klag]:   https://cogeometry.com/study-group/geomsat.html
[motsem]: https://aaronwild.gitlab.io/mixedhodge23/motivic-hodge.pdf
[huysem]: https://www.math.uni-bonn.de/people/huybrech/SeminarMA.pdf
[galgerb]:  https://aaronwild.gitlab.io/galoisgerbe23/galoisgerbe.pdf
[waldhausen]:  https://aaronwild.gitlab.io/2segal/waldhausen.pdf
