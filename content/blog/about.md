+++
title = "about this blog"
template = "writing.html"
date = "2022-09-27"
+++


this is my attempt at constructing a website where i actually understand how the templates work.
mostly, i have shamelessly copied the gitlab-repo of [cionx.gitlab.io](https://cionx.gitlab.io) and mutilated it until i understood it.
things will probably become nicer in the near future.
i've also copied some stuff from the [lightspeed theme](https://github.com/carpetscheme/lightspeed).

the color scheme is based on the [nord color scheme](https://www.nordtheme.com/)
